# README #

### Compte: ###
* Module de connexion
* Module de rafraîchissement du token

### Produit : ###
* Module d’affichage des produits par famille 
* Module d’affichage des produits par sous-famille avec liste des produits contenu
* Module détail d’un produit avec liste des produits dans la même sous-famille ainsi que la possibilité de l’ajouter au panier

### Panier : ###
* Module récupération du panier courant
* Module mise à jour du panier courant
* Module validation du panier courant
* Module récupération des anciens panier

### Version: ###
* Module de version afin de faire un rafraichissement des données avec le minimum de données possible