package com.projet.zky.endpoint;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;
import com.projet.zky.adapter.FamilyAdapter;
import com.projet.zky.interfaces.Endpoint;
import com.projet.zky.model.ModelData;

import java.util.List;

/**
 * Created by KevinVi on 31/05/2017.
 */

public class FamilyEndpoint implements Endpoint {
    public static String ENDPOINT_NAME = "FamilyEndpoint";
    private List<ModelData> modelData;

    public FamilyEndpoint(List<ModelData> modelData) {
        this.modelData = modelData;
    }

    @Override
    public String getName() {
       return ENDPOINT_NAME;
    }

    @Override
    public void callData(View rootView, Fragment fragmentActivity) {
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FamilyAdapter familyAdapter = new FamilyAdapter(modelData);
                recyclerView.setAdapter(familyAdapter);
            }
        });
    }
}
