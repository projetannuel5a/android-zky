package com.projet.zky.endpoint;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;
import com.projet.zky.adapter.DetailAdapter;
import com.projet.zky.interfaces.Endpoint;
import com.projet.zky.model.ModelProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 31/05/2017.
 */

public class DetailEndpoint implements Endpoint {
    public static String ENDPOINT_NAME = "DetailEndpoint";
    private ModelProduct product;
    private List<ModelProduct> products = new ArrayList<>();

    public DetailEndpoint( ModelProduct product,List<ModelProduct> products) {
        this.product = product;
        this.products = products;
    }

    @Override
    public String getName() {
        return ENDPOINT_NAME;
    }

    @Override
    public void callData(final View rootView, Fragment fragmentActivity) {
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DetailAdapter detailAdapter = new DetailAdapter(product, products, rootView.getContext());
                recyclerView.setAdapter(detailAdapter);
            }
        });
    }

}
