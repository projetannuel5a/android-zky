package com.projet.zky.endpoint;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.projet.zky.R;
import com.projet.zky.adapter.SearchProductAdapter;
import com.projet.zky.interfaces.Endpoint;
import com.projet.zky.model.ModelData;
import com.projet.zky.model.ModelProduct;
import com.projet.zky.model.ModelSubFamilyPublic;
import com.projet.zky.util.NullAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 09/07/2017.
 */

public class SearchEndpoint implements Endpoint {
    public static String ENDPOINT_NAME = "SearchEndpoint";
    private List<ModelData> modelData;

    public SearchEndpoint(List<ModelData> modelData) {
        this.modelData = modelData;
    }

    @Override
    public String getName() {
        return ENDPOINT_NAME;
    }

    @Override
    public void callData(final View rootView, Fragment fragmentActivity) {

        ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                final Spinner spinnerFamily = (Spinner) rootView.findViewById(R.id.spinner_family);
                final Spinner spinnerSubFamily = (Spinner) rootView.findViewById(R.id.spinner_subfamily);
                final SearchView searchView = (SearchView) rootView.findViewById(R.id.search_view);
                final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_search);
                final List<ModelProduct> tempProducts = new ArrayList<>();
                final List<String> familiesName = new ArrayList<>();
                familiesName.add(rootView.getContext().getString(R.string.all_families));
                for (ModelData m :
                        modelData) {
                    familiesName.add(m.getFamily().getName());
                    for (ModelSubFamilyPublic msfp :
                            m.getSubfamilyPublics()) {
                        tempProducts.addAll(msfp.getProducts());
                    }
                }

                ArrayAdapter<String> familyAdapter = new ArrayAdapter<>(rootView.getContext(), android.R.layout.simple_spinner_item, familiesName);
                familyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                familyAdapter.notifyDataSetChanged();
                spinnerFamily.setAdapter(familyAdapter);
                spinnerFamily.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        List<String> subFamiliesName = new ArrayList<>();

                        subFamiliesName.add(rootView.getContext().getString(R.string.all_sub_families));
                        if (position > 0) {
                            position--;
                            for (ModelSubFamilyPublic msfp :
                                    modelData.get(position).getSubfamilyPublics()) {
                                subFamiliesName.add(msfp.getSubFamily().getName());
                            }
                        }
                        ArrayAdapter<String> subFamilyAdapter = new ArrayAdapter<>(rootView.getContext(), android.R.layout.simple_spinner_item, subFamiliesName);
                        subFamilyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        subFamilyAdapter.notifyDataSetChanged();
                        spinnerSubFamily.setAdapter(subFamilyAdapter);
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                recyclerView.setAdapter(new NullAdapter());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext());
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        final List<ModelProduct> products = new ArrayList<>();

                        if (spinnerFamily.getSelectedItemPosition() == 0) {
                            if (!query.isEmpty()) {
                                for (ModelProduct p :
                                        tempProducts) {
                                    if (p.getCodearticle().toLowerCase().contains(query.toLowerCase()) ||
                                            p.getLibelleinterne().toLowerCase().contains(query.toLowerCase()) ||
                                            p.getIntitule1().toLowerCase().contains(query.toLowerCase())) {
                                        products.add(p);
                                    }
                                }
                            }
                        } else {
                            String familyName = spinnerFamily.getSelectedItem().toString();
                            String subFamilyName = spinnerSubFamily.getSelectedItem().toString();
                            for (ModelData m :
                                    modelData) {
                                if (familyName.equals(m.getFamily().getName())) {
                                    for (ModelSubFamilyPublic msfp :
                                            m.getSubfamilyPublics()) {
                                        if (spinnerSubFamily.getSelectedItemPosition() == 0) {
                                            if (!query.isEmpty()) {
                                                for (ModelProduct p :
                                                        msfp.getProducts()) {
                                                    if (p.getCodearticle().toLowerCase().contains(query.toLowerCase()) ||
                                                            p.getLibelleinterne().toLowerCase().contains(query.toLowerCase()) ||
                                                            p.getIntitule1().toLowerCase().contains(query.toLowerCase())) {
                                                        products.add(p);
                                                    }
                                                }
                                            }
                                        } else if (subFamilyName.equals(msfp.getSubFamily().getName())) {
                                            if (!query.isEmpty()) {
                                                for (ModelProduct p :
                                                        msfp.getProducts()) {
                                                    if (p.getCodearticle().toLowerCase().contains(query.toLowerCase()) ||
                                                            p.getLibelleinterne().toLowerCase().contains(query.toLowerCase()) ||
                                                            p.getIntitule1().toLowerCase().contains(query.toLowerCase())) {
                                                        products.add(p);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        recyclerView.setAdapter(new SearchProductAdapter(products, rootView.getContext()));
                        return false;
                    }
                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
            }
        });
    }
}