package com.projet.zky.endpoint;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;
import com.projet.zky.adapter.SubfamilyAdapter;
import com.projet.zky.interfaces.Endpoint;
import com.projet.zky.model.ModelSubFamilyPublic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KevinVi on 06/04/2017.
 */

public class SubFamilyEndpoint implements Endpoint {
  public static String ENDPOINT_NAME = "SubFamilyEndpoint";

  private List<ModelSubFamilyPublic> subfamily = new ArrayList<>();

  public SubFamilyEndpoint(List<ModelSubFamilyPublic> subfamily) {
    this.subfamily = subfamily;
  }

  @Override
  public String getName() {
    return ENDPOINT_NAME;
  }


  @Override
  public void callData(final View rootView, Fragment fragmentActivity) {

    final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
    ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
      @Override
      public void run() {
        SubfamilyAdapter subfamilyAdapter = new SubfamilyAdapter(rootView.getContext(), subfamily);
        recyclerView.setAdapter(subfamilyAdapter);
      }
    });
  }

}
