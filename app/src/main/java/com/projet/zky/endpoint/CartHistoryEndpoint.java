package com.projet.zky.endpoint;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.projet.zky.R;
import com.projet.zky.adapter.CartHistoryAdapter;
import com.projet.zky.interfaces.Endpoint;
import com.projet.zky.interfaces.EndpointInterface;
import com.projet.zky.model.ModelCartHistory;
import com.projet.zky.util.Shared;
import com.projet.zky.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.projet.zky.util.Utils.isOnline;

/**
 * Created by Kevin on 10/07/2017.
 */

public class CartHistoryEndpoint implements Endpoint {
    private static final String CART = "carthistory";
    public static String ENDPOINT_NAME = "CartHistoryEndpoint";

    @Override
    public String getName() {
        return ENDPOINT_NAME;
    }

    @Override
    public void callData(View rootView, Fragment fragmentActivity) {
    }

    public Call callData(final View rootView, Fragment fragmentActivity, Call call) {

        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progress_cart);
        progressBar.setVisibility(View.VISIBLE);
        final AppCompatTextView centerMsg = (AppCompatTextView) rootView.findViewById(R.id.center_message);
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        if (isOnline(fragmentActivity.getContext())) {
            String url = fragmentActivity.getString(R.string.url_data);
            Retrofit.Builder retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Utils.baseConnection(fragmentActivity.getContext(), CART));
            EndpointInterface endpointInterface = retrofit.build().create(EndpointInterface.class);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("token", Shared.getToken(fragmentActivity.getContext()));
            Call<List<ModelCartHistory>> call2 = endpointInterface.getCartHistory(jsonObject);

            call2.enqueue(new Callback<List<ModelCartHistory>>() {
                @Override
                public void onResponse(Call<List<ModelCartHistory>> call, final Response<List<ModelCartHistory>> response) {
                    if (response.isSuccessful()) {
                        progressBar.setVisibility(View.GONE);
                        if (response.body().isEmpty()) {
                            centerMsg.setText(R.string.empty_cart);
                            centerMsg.setVisibility(View.VISIBLE);
                        } else {
                            ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    CartHistoryAdapter cartHistoryAdapter = new CartHistoryAdapter(response.body(), rootView.getContext());
                                    recyclerView.setAdapter(cartHistoryAdapter);
                                }
                            });
                        }
                    } else {
                        ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                centerMsg.setText(R.string.empty_cart);
                                progressBar.setVisibility(View.GONE);
                                centerMsg.setVisibility(View.VISIBLE);
                                CartHistoryAdapter cartHistoryAdapter = new CartHistoryAdapter(new ArrayList<ModelCartHistory>(), rootView.getContext());
                                recyclerView.setAdapter(cartHistoryAdapter);
                            }
                        });
                    }
                }
                @Override
                public void onFailure(Call<List<ModelCartHistory>> call, Throwable t) {
                    ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);
                            centerMsg.setText(R.string.empty_cart);
                            centerMsg.setVisibility(View.VISIBLE);
                            CartHistoryAdapter cartHistoryAdapter = new CartHistoryAdapter(new ArrayList<ModelCartHistory>(), rootView.getContext());
                            recyclerView.setAdapter(cartHistoryAdapter);
                        }
                    });
                }
            });
            call = call2;
        } else {
            centerMsg.setText(R.string.internet);
            progressBar.setVisibility(View.GONE);
            centerMsg.setVisibility(View.VISIBLE);
        }
        return call;
    }
}
