package com.projet.zky.endpoint;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.projet.zky.R;
import com.projet.zky.adapter.CartAdapter;
import com.projet.zky.interfaces.Endpoint;
import com.projet.zky.interfaces.EndpointInterface;
import com.projet.zky.model.ModelCartHistory;
import com.projet.zky.util.Shared;
import com.projet.zky.util.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.projet.zky.util.Utils.isOnline;

/**
 * Created by KevinVi on 20/06/2017 for ZKY.
 */

public class CartEndpoint implements Endpoint {
    private static final String CART = "cart";
    public static String ENDPOINT_NAME = "CartEndpoint";


    @Override
    public String getName() {
        return ENDPOINT_NAME;
    }


    @Override
    public void callData(final View rootView, Fragment fragmentActivity) {

    }

    public Call callData(final View rootView, Fragment fragmentActivity, Call call) {

        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progress_cart);
        final AppCompatTextView tv = (AppCompatTextView) rootView.findViewById(R.id.total_cart);
        final AppCompatTextView centerMsg = (AppCompatTextView) rootView.findViewById(R.id.center_message);
        final AppCompatButton button = (AppCompatButton) rootView.findViewById(R.id.validate_cart);


        if (isOnline(fragmentActivity.getContext())) {
            String url = fragmentActivity.getString(R.string.url_data);
            Retrofit.Builder retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Utils.baseConnection(fragmentActivity.getContext(), CART));
            EndpointInterface endpointInterface = retrofit.build().create(EndpointInterface.class);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("token", Shared.getToken(fragmentActivity.getContext()));
            Call<ModelCartHistory> call2 = endpointInterface.getCurrentCart(jsonObject);

            call2.enqueue(new Callback<ModelCartHistory>() {
                @Override
                public void onResponse(Call<ModelCartHistory> call, final Response<ModelCartHistory> response) {


                    if (response.isSuccessful()) {
                        ((Activity) rootView.getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                progressBar.setVisibility(View.GONE);
                                if (!response.body().getCartLinePublics().isEmpty()) {
                                    CartAdapter cartAdapter = new CartAdapter(response.body(), rootView.getContext(), tv, button);
                                    recyclerView.setAdapter(cartAdapter);
                                } else {
                                    centerMsg.setText(R.string.empty_cart);
                                    centerMsg.setVisibility(View.VISIBLE);
                                    button.setVisibility(View.GONE);
                                    tv.setVisibility(View.GONE);
                                }
                            }
                        });
                    } else {
                        progressBar.setVisibility(View.GONE);
                        centerMsg.setText(R.string.impossible_cart);
                        centerMsg.setVisibility(View.VISIBLE);
                        button.setVisibility(View.GONE);
                        tv.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ModelCartHistory> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    centerMsg.setText(R.string.impossible_cart);
                    centerMsg.setVisibility(View.VISIBLE);
                    button.setVisibility(View.GONE);
                    tv.setVisibility(View.GONE);
                }
            });

            call = call2;
        } else {

            progressBar.setVisibility(View.GONE);
            centerMsg.setText(R.string.internet);
            centerMsg.setVisibility(View.VISIBLE);
            button.setVisibility(View.GONE);
            tv.setVisibility(View.GONE);
        }
        return call;
    }

}
