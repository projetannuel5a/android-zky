package com.projet.zky.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.anupcowkur.reservoir.Reservoir;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.projet.zky.R;
import com.projet.zky.interfaces.EndpointInterface;
import com.projet.zky.model.ModelData;
import com.projet.zky.model.ModelFamily;
import com.projet.zky.model.ModelProduct;
import com.projet.zky.model.ModelSubFamily;
import com.projet.zky.model.ModelSubFamilyPublic;
import com.projet.zky.model.ModelVersion;
import com.projet.zky.util.Shared;
import com.projet.zky.util.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends AppCompatActivity {

    public static final String RESERVOIR_PRODUCT = "product";
    private final int TIME_LOADER = 2000;
    private final int TIME_LOADER_FAIL = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        try {
            Reservoir.init(getApplicationContext(), 50000000); //in bytes
        } catch (IOException e) {
            //failure
        }
        callData();

    }

    void callData() {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .client(Utils.baseConnection(getApplicationContext(), RESERVOIR_PRODUCT))
                .baseUrl(getString(R.string.url_data))
                .addConverterFactory(GsonConverterFactory.create());
        EndpointInterface getInterface = retrofit.build().create(EndpointInterface.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("version", Shared.getVersion(getApplicationContext()));
        Call<ModelVersion> call = getInterface.getVerson(jsonObject);
        final Type resultType = new TypeToken<List<ModelData>>() {
        }.getType();
        List<ModelData> data = null;
        try {
            data = Reservoir.get(RESERVOIR_PRODUCT, resultType);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
        final List<ModelData> finalData = data;
        call.enqueue(new Callback<ModelVersion>() {
            @Override
            public void onResponse(Call<ModelVersion> call, Response<ModelVersion> response) {
                if (response.isSuccessful()) {
                    List<ModelData> datas;
                    if (finalData == null) {
                        datas = createData(response.body().getFamilyList(), response.body().getSubFamilyList(), response.body().getProductList());
                    } else {
                        datas = updateData(response.body().getFamilyList(), response.body().getSubFamilyList(), response.body().getProductList(), finalData);
                    }
                    Shared.saveVersion(getApplicationContext(), response.body().getVersion());

                    try {
                        Reservoir.put(RESERVOIR_PRODUCT, datas);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                    }, TIME_LOADER);
                } else {
                    if (finalData != null) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                            }
                        }, TIME_LOADER);
                    } else {
                        Toast.makeText(SplashActivity.this, R.string.msg_internet, Toast.LENGTH_LONG).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();

                            }
                        }, TIME_LOADER_FAIL);
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelVersion> call, Throwable t) {
                if (finalData != null) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                    }, TIME_LOADER);
                } else {
                    Toast.makeText(SplashActivity.this, R.string.msg_internet, Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();

                        }
                    }, TIME_LOADER_FAIL);
                }

            }
        });
    }

    private List<ModelData> createData(List<ModelFamily> families, List<ModelSubFamily> subFamilies, List<ModelProduct> products) {
        List<ModelData> datas = new ArrayList<>();
        for (ModelFamily f :
                families) {
            List<ModelSubFamilyPublic> modelSubFamilies = new ArrayList<>();

            for (ModelSubFamily s :
                    subFamilies) {
                if (s.getFamily().getName().equals(f.getName())) {
                    List<ModelProduct> modelProducts = new ArrayList<>();
                    for (ModelProduct p :
                            products) {
                        if (p.getFamilyName().equals(f.getName()) && p.getSubFamilyName().equals(s.getName())) {
                            modelProducts.add(p);
                        }
                    }
                    modelSubFamilies.add(new ModelSubFamilyPublic(s, modelProducts));
                }
            }
            datas.add(new ModelData(f, modelSubFamilies));
        }
        return datas;
    }

    private List<ModelData> updateData(List<ModelFamily> families, List<ModelSubFamily> subFamilies, List<ModelProduct> products, List<ModelData> data) {


        //Add new products only
        if (subFamilies == null) {
            for (ModelData f :
                    data) {
                for (ModelSubFamilyPublic s :
                        f.getSubfamilyPublics()) {
                    if (s.getSubFamily().getName().equals(f.getFamily().getName())) {
                        List<ModelProduct> modelProducts = s.getProducts();
                        for (ModelProduct p :
                                products) {
                            if (p.getFamilyName().equals(f.getFamily().getName()) && p.getSubFamilyName().equals(s.getSubFamily().getName())) {
                                modelProducts.add(p);
                            }
                        }
                        s.setProducts(modelProducts);
                    }
                }
            }
            return data;
        }

        //Add subfamilies and products
        if (families == null) {
            for (ModelData f :
                    data) {
                List<ModelSubFamilyPublic> modelSubFamilies = new ArrayList<>();
                for (ModelSubFamily s :
                        subFamilies) {
                    if (s.getFamily().getName().equals(f.getFamily().getName())) {
                        List<ModelProduct> modelProducts = new ArrayList<>();
                        for (ModelProduct p :
                                products) {
                            if (p.getFamilyName().equals(f.getFamily().getName()) && p.getSubFamilyName().equals(s.getName())) {
                                modelProducts.add(p);
                            }
                        }
                        modelSubFamilies.add(new ModelSubFamilyPublic(s, modelProducts));
                    }
                }
                data.add(new ModelData(f.getFamily(), modelSubFamilies));
            }
            return data;
        }

        //Add all
        for (ModelFamily f :
                families) {
            List<ModelSubFamilyPublic> modelSubFamilies = new ArrayList<>();
            for (ModelSubFamily s :
                    subFamilies) {
                if (s.getFamily().getName().equals(f.getName())) {
                    List<ModelProduct> modelProducts = new ArrayList<>();
                    for (ModelProduct p :
                            products) {
                        if (p.getFamilyName().equals(f.getName()) && p.getSubFamilyName().equals(s.getName())) {
                            modelProducts.add(p);
                        }
                    }
                    modelSubFamilies.add(new ModelSubFamilyPublic(s, modelProducts));
                }
            }
            data.add(new ModelData(f, modelSubFamilies));
        }
        return data;
    }
}
