package com.projet.zky.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.anupcowkur.reservoir.Reservoir;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.projet.zky.R;
import com.projet.zky.endpoint.CartEndpoint;
import com.projet.zky.endpoint.CartHistoryEndpoint;
import com.projet.zky.endpoint.DetailEndpoint;
import com.projet.zky.endpoint.FamilyEndpoint;
import com.projet.zky.endpoint.SearchEndpoint;
import com.projet.zky.fragment.PlaceholderFragment;
import com.projet.zky.interfaces.Login;
import com.projet.zky.model.ModelData;
import com.projet.zky.model.ModelLogin;
import com.projet.zky.util.Shared;
import com.projet.zky.util.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.projet.zky.activity.SplashActivity.RESERVOIR_PRODUCT;
import static com.projet.zky.util.Shared.saveLoginInfo;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHENTICATE = "authenticate";
    FrameLayout frameLayout;
    List<ModelData> modelData;
    MenuItem item;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setFragment(PlaceholderFragment.newInstance(new FamilyEndpoint(modelData)));
                    return true;
                case R.id.navigation_search:
                    setFragment(PlaceholderFragment.newInstance(new SearchEndpoint(modelData)));
                    return true;
                case R.id.navigation_cart:
                    setFragment(PlaceholderFragment.newInstance(new CartEndpoint()));
                    return true;
                case R.id.navigation_history:
                    setFragment(PlaceholderFragment.newInstance(new CartHistoryEndpoint()));
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Type resultType = new TypeToken<List<ModelData>>() {
        }.getType();
        try {
            modelData = Reservoir.get(RESERVOIR_PRODUCT, resultType);
        } catch (NullPointerException | IllegalStateException | IOException e) {
            e.printStackTrace();
            try {
                Reservoir.init(getApplicationContext(), 50000000); //in bytes
                modelData = Reservoir.get(RESERVOIR_PRODUCT, resultType);
            } catch (IOException ex) {
                //failure
            }
        }
        invalidateOptionsMenu();

        frameLayout = (FrameLayout) findViewById(R.id.content);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_home);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);

        item = menu.findItem(R.id.action_compte);
        if (Shared.getToken(getApplicationContext()).isEmpty()) {

            menu.findItem(R.id.action_compte).setVisible(true);
        } else {
            menu.findItem(R.id.action_compte).setVisible(false);


        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_compte) {
            LayoutInflater li = LayoutInflater.from(this);
            View prompt = li.inflate(R.layout.row_login, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setView(prompt);


            final TextInputLayout loginIdInput;
            final TextInputLayout loginPassInput;
            final AppCompatButton loginBtn;

            final AppCompatEditText loginId = (AppCompatEditText) prompt.findViewById(R.id.login_id_input);
            loginId.setText("admin");
            final AppCompatEditText loginPass = (AppCompatEditText) prompt.findViewById(R.id.password_id_input);
            loginPass.setText("cXNH44GxgG");
            loginIdInput = (TextInputLayout) findViewById(R.id.id_layout);
            loginPassInput = (TextInputLayout) findViewById(R.id.id_layout_password);
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            List<String> listFragementName = new ArrayList<>();
            listFragementName.add(DetailEndpoint.ENDPOINT_NAME);
            listFragementName.add(CartEndpoint.ENDPOINT_NAME);

            Fragment f;
            for (String s : listFragementName) {
                f = getSupportFragmentManager().findFragmentByTag(s);
                if (f != null && f.isVisible()) {
                    break;
                }
            }


            alertDialogBuilder.setCancelable(false)
                    .setNeutralButton(getResources().getString(R.string.connection), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                //TODO verification empty

                                login(loginId.getText().toString(), loginPass.getText().toString());


                            } catch (Exception e) {

                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });


            alertDialogBuilder.show();

        }
        return super.onOptionsItemSelected(item);
    }

    protected void setFragment(Fragment fragment) {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment, ((PlaceholderFragment) fragment).getName());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(R.string.caution);
            alertDialog.setMessage(getResources().getString(R.string.leaving_app));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.cancel),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        } else {
            super.onBackPressed();
        }
    }


    private void login(final String login, final String password) {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .client(Utils.baseConnection(getApplicationContext(), AUTHENTICATE))
                .baseUrl(getString(R.string.url_login))
                .addConverterFactory(GsonConverterFactory.create());
        Login postInterface = retrofit.build().create(Login.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("login", login);
        jsonObject.addProperty("password", password);

        Call<ModelLogin> call = postInterface.getLogin(jsonObject);
        call.enqueue(new Callback<ModelLogin>() {
            @Override
            public void onResponse(Call<ModelLogin> call, Response<ModelLogin> response) {

                if (response.isSuccessful()) {
                    saveLoginInfo(getApplicationContext(), login, password);
                    Shared.saveToken(getApplicationContext(), response.body().token);

                    showOverflowMenu(false);
                    Toast.makeText(getApplicationContext(), R.string.logged, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.logged_fail, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ModelLogin> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.logged_fail, Toast.LENGTH_LONG).show();
            }

        });
    }


    public void showOverflowMenu(boolean showMenu) {
        if (item == null)
            return;
        item.setVisible(showMenu);
    }
}
