package com.projet.zky.viewHolders;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;

/**
 * Created by Kevin on 03/06/2017.
 */

public class DetailViewHolder extends RecyclerView.ViewHolder {
    private final AppCompatTextView textView;
    private final AppCompatButton addCart;
    private final AppCompatButton removeCart;
    private final AppCompatImageView imageDetail;
    private final RecyclerView recyclerView;
    private final AppCompatEditText quantity;
    private final AppCompatTextView price;
    private final AppCompatTextView total;
    private final AppCompatButton buttonCart;

    public DetailViewHolder(View itemView) {
        super(itemView);
        textView = (AppCompatTextView) itemView.findViewById(R.id.text_detail);
        addCart = (AppCompatButton) itemView.findViewById(R.id.button_add_detail);
        removeCart = (AppCompatButton) itemView.findViewById(R.id.button_remove_detail);
        imageDetail = (AppCompatImageView) itemView.findViewById(R.id.img_detail);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view_detail);
        quantity = (AppCompatEditText) itemView.findViewById(R.id.quantity_detail);
        price = (AppCompatTextView) itemView.findViewById(R.id.prix);
        total = (AppCompatTextView) itemView.findViewById(R.id.total);
        buttonCart = (AppCompatButton) itemView.findViewById(R.id.add_cart_button);

    }

    public AppCompatTextView getTextView() {
        return textView;
    }

    public AppCompatButton getAddCart() {
        return addCart;
    }

    public AppCompatImageView getImageDetail() {
        return imageDetail;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public AppCompatButton getRemoveCart() {
        return removeCart;
    }

    public AppCompatEditText getQuantity() {
        return quantity;
    }

    public AppCompatButton getButtonCart() {
        return buttonCart;
    }

    public AppCompatTextView getPrice() {
        return price;
    }

    public AppCompatTextView getTotal() {
        return total;
    }
}
