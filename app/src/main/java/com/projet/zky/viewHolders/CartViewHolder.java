package com.projet.zky.viewHolders;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;

/**
 * Created by Kevin on 20/06/2017.
 */

public class CartViewHolder extends RecyclerView.ViewHolder {

    private final AppCompatTextView titleCart;
    private final AppCompatButton addCart;
    private final AppCompatButton removeCart;
    private final AppCompatImageView imageCart;
    private final AppCompatEditText quantity;

    public CartViewHolder(View itemView) {
        super(itemView);
        titleCart = (AppCompatTextView) itemView.findViewById(R.id.title_cart);
        imageCart = (AppCompatImageView) itemView.findViewById(R.id.img_cart);
        removeCart = (AppCompatButton) itemView.findViewById(R.id.button_remove_cart);
        addCart = (AppCompatButton) itemView.findViewById(R.id.button_add_cart);
        quantity = (AppCompatEditText) itemView.findViewById(R.id.quantity_cart);
    }

    public AppCompatTextView getTitleCart() {
        return titleCart;
    }

    public AppCompatButton getAddCart() {
        return addCart;
    }

    public AppCompatButton getRemoveCart() {
        return removeCart;
    }

    public AppCompatImageView getImageCart() {
        return imageCart;
    }

    public AppCompatEditText getQuantity() {
        return quantity;
    }

}