package com.projet.zky.viewHolders;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;

/**
 * Created by Kevin on 09/07/2017.
 */

public class CartLineViewHolder extends RecyclerView.ViewHolder  {
    private final AppCompatTextView cartId;
    private final LinearLayoutCompat linearLayoutCompat;
    private final LinearLayoutCompat listCartLine;
    private final CardView cardView;

    public CartLineViewHolder(View itemView) {
        super(itemView);

        cartId = (AppCompatTextView) itemView.findViewById(R.id.cart_id);
        linearLayoutCompat = (LinearLayoutCompat) itemView.findViewById(R.id.linearLayoutCompatBottom);
        listCartLine = (LinearLayoutCompat) itemView.findViewById(R.id.list_cart_line);
        cardView = (CardView)itemView.findViewById(R.id.cardView);
    }

    public AppCompatTextView getCartId() {
        return cartId;
    }

    public LinearLayoutCompat getLinearLayoutCompat() {
        return linearLayoutCompat;
    }

    public LinearLayoutCompat getListCartLine() {
        return listCartLine;
    }

    public CardView getCardView() {
        return cardView;
    }

}
