package com.projet.zky.viewHolders;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;
import com.projet.zky.activity.MainActivity;
import com.projet.zky.endpoint.DetailEndpoint;
import com.projet.zky.fragment.PlaceholderFragment;
import com.projet.zky.model.ModelProduct;

import java.util.List;

/**
 * Created by Kevin on 09/07/2017.
 */

public class SearchProductViewHolder extends RecyclerView.ViewHolder {
    private final AppCompatTextView productName;
    private final AppCompatImageView imgProduct;

    public SearchProductViewHolder(View itemView) {
        super(itemView);
        productName = (AppCompatTextView) itemView.findViewById(R.id.search_product_name);
        imgProduct = (AppCompatImageView) itemView.findViewById(R.id.search_product_image);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = ((MainActivity) v.getContext()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =
                        fragmentManager.beginTransaction();
                fragmentTransaction.replace(android.R.id.content, PlaceholderFragment.newInstance(new DetailEndpoint((ModelProduct) getProductName().getTag(R.string.id_product), (List<ModelProduct>) getProductName().getTag(R.string.id_products))),DetailEndpoint.ENDPOINT_NAME);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    public AppCompatTextView getProductName() {
        return productName;
    }

    public AppCompatImageView getImgProduct() {
        return imgProduct;
    }
}
