package com.projet.zky.viewHolders;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;
import com.projet.zky.activity.MainActivity;
import com.projet.zky.endpoint.DetailEndpoint;
import com.projet.zky.fragment.PlaceholderFragment;
import com.projet.zky.model.ModelProduct;

import java.util.List;

/**
 * Created by KevinVi on 31/05/2017.
 */

public class SubFamilyItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final AppCompatTextView textView;
    private final AppCompatImageView imageView;

    public SubFamilyItemViewHolder(View itemView) {
        super(itemView);
        textView = (AppCompatTextView) itemView.findViewById(R.id.category_item);
        imageView =(AppCompatImageView) itemView.findViewById(R.id.img_product);
        itemView.setOnClickListener(this);
    }

    public AppCompatTextView getTextView() {
        return textView;
    }

    public AppCompatImageView getImageView() {
        return imageView;
    }

    @Override
    public void onClick(View view) {

        FragmentManager fragmentManager = ((MainActivity) view.getContext()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, PlaceholderFragment.newInstance(new DetailEndpoint((ModelProduct) getTextView().getTag(R.string.id_product), (List<ModelProduct>) getTextView().getTag(R.string.id_products))),DetailEndpoint.ENDPOINT_NAME);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}