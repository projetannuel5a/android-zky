package com.projet.zky.viewHolders;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;

/**
 * Created by KevinVi on 31/05/2017.
 */

public class SubFamilyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
  private final AppCompatTextView textView;
  private final RecyclerView recyclerView;

  public SubFamilyViewHolder(View itemView) {
    super(itemView);
    textView = (AppCompatTextView) itemView.findViewById(R.id.category);
    recyclerView =(RecyclerView) itemView.findViewById(R.id.recycler_view_categories);
  }

  public AppCompatTextView getTextView() {
    return textView;
  }

  public RecyclerView getRecyclerView() {
    return recyclerView;
  }

  @Override
  public void onClick(View view) {
  }
}
