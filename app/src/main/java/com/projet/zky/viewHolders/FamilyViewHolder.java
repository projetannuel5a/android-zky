package com.projet.zky.viewHolders;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.projet.zky.R;
import com.projet.zky.activity.MainActivity;
import com.projet.zky.endpoint.SubFamilyEndpoint;
import com.projet.zky.fragment.PlaceholderFragment;
import com.projet.zky.model.ModelSubFamilyPublic;

import java.util.List;

/**
 * Created by KevinVi on 31/05/2017.
 */

public class FamilyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

  private final AppCompatTextView textView;


  public FamilyViewHolder(View itemView) {
    super(itemView);
    textView = (AppCompatTextView) itemView.findViewById(R.id.catalogue);
    itemView.setOnClickListener(this);
  }

  public AppCompatTextView getTextView() {
    return textView;
  }

  @Override
  public void onClick(View view) {

    FragmentManager fragmentManager = ((MainActivity) view.getContext()).getSupportFragmentManager();
    FragmentTransaction fragmentTransaction =
        fragmentManager.beginTransaction();
    fragmentTransaction.replace(android.R.id.content, PlaceholderFragment.newInstance(new SubFamilyEndpoint((List<ModelSubFamilyPublic>) getTextView().getTag())),SubFamilyEndpoint.ENDPOINT_NAME);
    fragmentTransaction.addToBackStack(null);
    fragmentTransaction.commit();
  }
}
