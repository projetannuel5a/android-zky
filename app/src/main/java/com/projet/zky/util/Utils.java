package com.projet.zky.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import com.google.gson.JsonObject;
import com.projet.zky.BuildConfig;
import com.projet.zky.R;
import com.projet.zky.interfaces.Login;
import com.projet.zky.model.ModelLogin;

import java.io.File;
import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by KevinVi on 07/04/2017.
 */

public class Utils {

    public static OkHttpClient baseConnection(final Context context, final String cacheName) {

        File file = new File(context.getCacheDir(), cacheName);
        int cacheSize = getCacheFileSize();
        Cache cache = new Cache(file, cacheSize);
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                int maxStale = Integer.MAX_VALUE;
                return chain.proceed(
                        request.newBuilder()
                                .addHeader("Cache-Control", "public, max-stale=" + maxStale)
                                .build()
                );
            }
        };
        // Lib to change header fo cache
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        final Response response = chain.proceed(request);
                        if (response.code() == 403) {
                            OkHttpClient.Builder builder = new OkHttpClient.Builder();
                            builder.addInterceptor(new Interceptor() {
                                @Override
                                public Response intercept(Chain chain) throws IOException {
                                    Request request = chain.request().newBuilder()
                                            .build();
                                    return chain.proceed(request);
                                }
                            });
                            Retrofit.Builder retrofit = new Retrofit.Builder()
                                    .client(builder.build())
                                    .baseUrl(context.getString(R.string.url_login))
                                    .addConverterFactory(GsonConverterFactory.create());
                            Login postInterface = retrofit.build().create(Login.class);

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("login", Shared.getId(context));
                            jsonObject.addProperty("password", Shared.getPass(context));
                            Call<ModelLogin> call = postInterface.getLogin(jsonObject);
                            call.enqueue(new Callback<ModelLogin>() {
                                @Override
                                public void onResponse(Call<ModelLogin> call, retrofit2.Response<ModelLogin> res) {
                                    if (res.isSuccessful()) {
                                        Shared.saveToken(context, res.body().token);
                                        JsonObject body = new JsonObject();
                                        body.addProperty("token", res.body().token);
                                    }
                                }

                                @Override
                                public void onFailure(Call<ModelLogin> call, Throwable t) {
                                }
                            });

                        }
                        return response;
                    }
                })
                .authenticator(new Authenticator() {
                    @Override
                    public Request authenticate(Route route, final Response responses) throws IOException {
                        if (BuildConfig.DEBUG) {
                            Log.d(cacheName, "authenticate: " + responses.challenges());
                        }
                        OkHttpClient.Builder builder = new OkHttpClient.Builder();
                        builder.addInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request request = chain.request().newBuilder()
                                        .build();
                                return chain.proceed(request);
                            }
                        });
                        Retrofit.Builder retrofit = new Retrofit.Builder()
                                .client(builder.build())
                                .baseUrl(context.getString(R.string.url_login))
                                .addConverterFactory(GsonConverterFactory.create());
                        Login postInterface = retrofit.build().create(Login.class);

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("login", Shared.getId(context));
                        jsonObject.addProperty("password", Shared.getPass(context));
                        Call<ModelLogin> call = postInterface.getLogin(jsonObject);
                        retrofit2.Response<ModelLogin> response = call.execute();
                        if (response.isSuccessful()) {
                            Shared.saveToken(context, response.body().token);

                            JsonObject body = new JsonObject();
                            body.addProperty("token", response.body().token);
                            return responses.request().newBuilder()
                                    .build();
                        } else {
                            return null;
                        }
                    }
                })
                .build();
    }

    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private static Integer getCacheFileSize() {
        return 10 * 1024 * 1024;
    }
}
