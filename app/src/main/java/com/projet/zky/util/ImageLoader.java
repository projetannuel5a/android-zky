package com.projet.zky.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.projet.zky.R;

/**
 * Created by Kevin on 11/07/2017 for ZKY.
 */

public class ImageLoader {
    /**
     * Load image with Glide.
     *
     * @param context   context.
     * @param imageView imageView on the top of the layout.
     * @param path      url of the image.
     */
    public static void loadImage(final Context context, final ImageView imageView, String path) {
        Glide.with(context)
                .load(path)
                .placeholder(R.drawable.placeholder)
                .dontTransform()
                .centerCrop()
                .error(R.drawable.placeholder)
                .into(imageView);
    }

}
