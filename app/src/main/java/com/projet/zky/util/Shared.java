package com.projet.zky.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by KevinVi on 07/04/2017.
 */

public class Shared {

    private static final String PREF_LOGIN = "PREF_LOGIN";
    private static final String PREF_TOKEN = "PREF_TOKEN";
    private static final String PREF_VERSION = "VERSION";

    public static void saveLoginInfo(Context context, String idLogin, String passLogin) {
        final SharedPreferences loginPreferences = context.getSharedPreferences(PREF_LOGIN, Context.MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = loginPreferences.edit();
        prefsEditor.putString("idLogin", idLogin);
        prefsEditor.putString("passLogin", passLogin);
        prefsEditor.apply();
    }

    public static String getId(Context context) {
        final SharedPreferences loginPreferences = context.getSharedPreferences(PREF_LOGIN, Context.MODE_PRIVATE);
        return loginPreferences.getString("idLogin", "");
    }

    public static String getPass(Context context) {
        final SharedPreferences loginPreferences = context.getSharedPreferences(PREF_LOGIN, Context.MODE_PRIVATE);
        return loginPreferences.getString("passLogin", "");
    }

    public static void saveToken(Context context, String token) {
        final SharedPreferences loginPreferences = context.getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = loginPreferences.edit();
        prefsEditor.putString(PREF_TOKEN, token);
        prefsEditor.apply();
    }

    public static String getToken(Context context) {
        final SharedPreferences loginPreferences = context.getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
        return loginPreferences.getString(PREF_TOKEN, "");
    }

    public static void saveVersion(Context context, int version) {
        final SharedPreferences loginPreferences = context.getSharedPreferences(PREF_VERSION, Context.MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = loginPreferences.edit();
        prefsEditor.putInt(PREF_VERSION, version);
        prefsEditor.apply();
    }


    public static int getVersion(Context context) {
        final SharedPreferences loginPreferences = context.getSharedPreferences(PREF_VERSION, Context.MODE_PRIVATE);
        return loginPreferences.getInt(PREF_VERSION, 0);
    }
}
