package com.projet.zky.util;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.model.GlideUrl;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by KevinVi on 07/04/2017.
 */

public class GlideModule implements com.bumptech.glide.module.GlideModule {
  private final int SIZE = 10 * 1024 * 1024;
  @Override
  public void applyOptions(Context context, GlideBuilder builder) {
    // Apply options to the builder here.
    builder.setDiskCache(
        new InternalCacheDiskCacheFactory(context, SIZE));
  }


  @Override
  public void registerComponents(Context context, Glide glide) {
    // register ModelLoaders here.
    final OkHttpClient.Builder builder = new OkHttpClient.Builder();
    // set your timeout here
    builder.readTimeout(15, TimeUnit.SECONDS);
    builder.writeTimeout(15, TimeUnit.SECONDS);
    builder.connectTimeout(15, TimeUnit.SECONDS);
    glide.register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(builder.build()));
  }

}