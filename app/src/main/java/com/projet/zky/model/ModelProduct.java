package com.projet.zky.model;

/**
 * Created by Kevin on 07/07/2017.
 */

public class ModelProduct {
    private int id;
    private String codearticle;
    private String intitule1;
    private String prixdevente;
    private String libelleinterne;
    private String img;
    private String familyName;
    private String subFamilyName;


    @Override
    public String toString() {
        return "ModelProduct{" +
                "id=" + id +
                ", codearticle='" + codearticle + '\'' +
                ", intitule1='" + intitule1 + '\'' +
                ", prixdevente='" + prixdevente + '\'' +
                ", libelleinterne='" + libelleinterne + '\'' +
                ", img='" + img + '\'' +
                ", familyName='" + familyName + '\'' +
                ", subFamilyName='" + subFamilyName + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getCodearticle() {
        return codearticle;
    }

    public String getIntitule1() {
        return intitule1;
    }

    public String getPrixdevente() {
        return prixdevente;
    }

    public String getLibelleinterne() {
        return libelleinterne;
    }

    public String getImg() {
        return img;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getSubFamilyName() {
        return subFamilyName;
    }
}
