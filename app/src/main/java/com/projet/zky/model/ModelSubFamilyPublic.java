package com.projet.zky.model;

import java.util.List;

/**
 * Created by Kevin on 07/07/2017.
 */

public class ModelSubFamilyPublic {
    private ModelSubFamily subFamily;

    private List<ModelProduct> products;


    @Override
    public String toString() {
        return "ModelSubFamilyPublic{" +
                "subFamily=" + subFamily +
                ", products=" + products +
                '}';
    }

    public ModelSubFamilyPublic(ModelSubFamily subFamily, List<ModelProduct> products) {
        this.subFamily = subFamily;
        this.products = products;
    }

    public ModelSubFamily getSubFamily() {
        return subFamily;
    }

    public List<ModelProduct> getProducts() {
        return products;
    }

    public void setSubFamily(ModelSubFamily subFamily) {
        this.subFamily = subFamily;
    }

    public void setProducts(List<ModelProduct> products) {
        this.products = products;
    }
}
