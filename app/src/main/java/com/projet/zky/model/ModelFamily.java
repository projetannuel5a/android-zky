package com.projet.zky.model;

/**
 * Created by leolebogoss on 06/04/2017.
 */

public class ModelFamily {
    private int id;
    private String name;
    private int family;

    @Override
    public String toString() {
        return "ModelFamily{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", family=" + family +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getFamily() {
        return family;
    }
}
