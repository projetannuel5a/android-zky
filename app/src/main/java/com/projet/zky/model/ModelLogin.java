package com.projet.zky.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by KevinVi on 06/04/2017.
 */

public class ModelLogin {
  @SerializedName("token")
  @Expose
  public String token;
}
