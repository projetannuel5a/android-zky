package com.projet.zky.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kevin on 07/07/2017.
 */

public class ModelVersion {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("version")
    @Expose
    private int version;
    @SerializedName("productList")
    @Expose
    private List<ModelProduct> productList;
    @SerializedName("familyList")
    @Expose
    private List<ModelFamily> familyList;
    @SerializedName("subFamilyList")
    @Expose
    private List<ModelSubFamily> subFamilyList;

    public int getId() {
        return id;
    }

    public int getVersion() {
        return version;
    }

    public List<ModelProduct> getProductList() {
        return productList;
    }

    public List<ModelFamily> getFamilyList() {
        return familyList;
    }

    public List<ModelSubFamily> getSubFamilyList() {
        return subFamilyList;
    }
}
