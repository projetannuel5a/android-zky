package com.projet.zky.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kevin on 09/07/2017.
 */

public class CartLinePublic {
    @SerializedName("quantity")
    @Expose
    private int quantity;

    @SerializedName("productPublics")
    @Expose
    private ModelProduct productPublics;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ModelProduct getProductPublics() {
        return productPublics;
    }

    public void setProductPublics(ModelProduct productPublics) {
        this.productPublics = productPublics;
    }
}
