package com.projet.zky.model;

import java.util.List;

/**
 * Created by Kevin on 07/07/2017.
 */

public class ModelData {
    private ModelFamily family;
    private List<ModelSubFamilyPublic> subfamilyPublics;


    @Override
    public String toString() {
        return "ModelData{" +
                "family=" + family +
                ", subfamilyPublics=" + subfamilyPublics +
                '}';
    }

    public ModelData(ModelFamily family, List<ModelSubFamilyPublic> subfamilyPublics) {
        this.family = family;
        this.subfamilyPublics = subfamilyPublics;
    }

    public ModelFamily getFamily() {
        return family;
    }

    public List<ModelSubFamilyPublic> getSubfamilyPublics() {
        return subfamilyPublics;
    }
}
