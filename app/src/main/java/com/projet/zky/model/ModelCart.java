package com.projet.zky.model;

/**
 * Created by leolebogoss on 06/04/2017.
 */

public class ModelCart {

  private static final long serialVersionUID = 1L;
  public String name;
  public int quantity;

  public ModelCart(String name, int quantity) {
    this.name = name;
    this.quantity = quantity;
  }

  @Override
  public String toString() {
    return "ModelCart{" +
        "name='" + name + '\'' +
        ", quantity=" + quantity +
        '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
}
