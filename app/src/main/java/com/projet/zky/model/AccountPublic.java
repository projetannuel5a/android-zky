package com.projet.zky.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kevin on 09/07/2017.
 */

public class AccountPublic {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("numDossier")
    @Expose
    private String numDossier;
    @SerializedName("nom")
    @Expose
    private String nom;
    @SerializedName("cp")
    @Expose
    private String cp;
    @SerializedName("ville")
    @Expose
    private String ville;
    @SerializedName("pays")
    @Expose
    private String pays;
    @SerializedName("enseigne")
    @Expose
    private String enseigne;
    @SerializedName("telephone1")
    @Expose
    private String telephone1;
    @SerializedName("telephone2")
    @Expose
    private String telephone2;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("administration")
    @Expose
    private String administration;
    @SerializedName("compte")
    @Expose
    private String compte;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("siret")
    @Expose
    private String siret;
    @SerializedName("numVoie")
    @Expose
    private String numVoie;
    @SerializedName("nomdelavoie")
    @Expose
    private String complementAdresse;
    @SerializedName("complementAdresse")
    @Expose
    private String nomdelavoie;
    @SerializedName("admin")
    @Expose
    private Boolean admin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNumDossier() {
        return numDossier;
    }

    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getEnseigne() {
        return enseigne;
    }

    public void setEnseigne(String enseigne) {
        this.enseigne = enseigne;
    }

    public String getTelephone1() {
        return telephone1;
    }

    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAdministration() {
        return administration;
    }

    public void setAdministration(String administration) {
        this.administration = administration;
    }

    public String getCompte() {
        return compte;
    }

    public void setCompte(String compte) {
        this.compte = compte;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getNumVoie() {
        return numVoie;
    }

    public void setNumVoie(String numVoie) {
        this.numVoie = numVoie;
    }

    public String getComplementAdresse() {
        return complementAdresse;
    }

    public void setComplementAdresse(String complementAdresse) {
        this.complementAdresse = complementAdresse;
    }

    public String getNomdelavoie() {
        return nomdelavoie;
    }

    public void setNomdelavoie(String nomdelavoie) {
        this.nomdelavoie = nomdelavoie;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
