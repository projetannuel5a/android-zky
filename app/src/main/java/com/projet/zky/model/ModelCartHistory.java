package com.projet.zky.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kevin on 09/07/2017.
 */

public class ModelCartHistory {

    @SerializedName("cart")
    @Expose
    private Cart cart;

    @SerializedName("accountPublic")
    @Expose
    private AccountPublic accountPublic;

    @SerializedName("cartLinePublics")
    @Expose
    private List<CartLinePublic> cartLinePublics;

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public AccountPublic getAccountPublic() {
        return accountPublic;
    }

    public void setAccountPublic(AccountPublic accountPublic) {
        this.accountPublic = accountPublic;
    }

    public List<CartLinePublic> getCartLinePublics() {
        return cartLinePublics;
    }

    public void setCartLinePublics(List<CartLinePublic> cartLinePublics) {
        this.cartLinePublics = cartLinePublics;
    }
}
