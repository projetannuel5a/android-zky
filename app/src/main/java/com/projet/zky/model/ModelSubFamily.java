package com.projet.zky.model;

/**
 * Created by Kevin on 07/07/2017.
 */

public class ModelSubFamily {

    private int id;

    private String name;

    private int subfamily;

    private ModelFamily family;


    @Override
    public String toString() {
        return "ModelSubFamily{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", subfamily=" + subfamily +
                ", family=" + family +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSubfamily() {
        return subfamily;
    }

    public ModelFamily getFamily() {
        return family;
    }
}
