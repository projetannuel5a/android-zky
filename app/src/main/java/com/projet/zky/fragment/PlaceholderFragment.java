package com.projet.zky.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anupcowkur.reservoir.Reservoir;
import com.google.gson.reflect.TypeToken;
import com.projet.zky.R;
import com.projet.zky.endpoint.CartEndpoint;
import com.projet.zky.endpoint.CartHistoryEndpoint;
import com.projet.zky.endpoint.FamilyEndpoint;
import com.projet.zky.endpoint.SearchEndpoint;
import com.projet.zky.interfaces.Endpoint;
import com.projet.zky.model.ModelData;
import com.projet.zky.util.NullAdapter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.projet.zky.activity.SplashActivity.RESERVOIR_PRODUCT;

/**
 * Created by KevinVi on 06/04/2017.
 */

public class PlaceholderFragment extends Fragment {
    private Endpoint endpoint;

    private Call call;

    public static PlaceholderFragment newInstance(Endpoint endpoint) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        fragment.setEndpoint(endpoint);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View rootView;
        if (endpoint.getName().equals(CartEndpoint.ENDPOINT_NAME)) {
            rootView = inflater.inflate(R.layout.fragment_cart, container, false);
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
            recyclerView.setAdapter(new NullAdapter());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setHasFixedSize(true);
            if (endpoint == null) {
                Type resultType = new TypeToken<List<ModelData>>() {
                }.getType();
                List<ModelData> modelData = new ArrayList<>();
                try {
                    modelData = Reservoir.get(RESERVOIR_PRODUCT, resultType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                endpoint = new FamilyEndpoint(modelData);
            }
            recyclerView.setLayoutManager(linearLayoutManager);
            call = ((CartEndpoint) endpoint).callData(rootView, this, call);
        } else if (endpoint.getName().equals(SearchEndpoint.ENDPOINT_NAME)) {
            rootView = inflater.inflate(R.layout.row_search_endpoint, container, false);
            endpoint.callData(rootView, this);
        } else if (endpoint.getName().equals(CartHistoryEndpoint.ENDPOINT_NAME)) {
            rootView = inflater.inflate(R.layout.fragment_placeholder, container, false);
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
            recyclerView.setAdapter(new NullAdapter());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setHasFixedSize(true);
            if (endpoint == null) {
                Type resultType = new TypeToken<List<ModelData>>() {
                }.getType();
                List<ModelData> modelData = new ArrayList<>();
                try {
                    modelData = Reservoir.get(RESERVOIR_PRODUCT, resultType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                endpoint = new FamilyEndpoint(modelData);
            }
            recyclerView.setLayoutManager(linearLayoutManager);
            call = ((CartHistoryEndpoint) endpoint).callData(rootView, this, call);
        } else {
            rootView = inflater.inflate(R.layout.fragment_placeholder, container, false);
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
            recyclerView.setAdapter(new NullAdapter());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setHasFixedSize(true);
            if (endpoint == null) {
                Type resultType = new TypeToken<List<ModelData>>() {
                }.getType();
                List<ModelData> modelData = new ArrayList<>();
                try {
                    modelData = Reservoir.get(RESERVOIR_PRODUCT, resultType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                endpoint = new FamilyEndpoint(modelData);
            }
            recyclerView.setLayoutManager(linearLayoutManager);
            endpoint.callData(rootView, this);

        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public void onDestroyView() {
        if (call != null) {
            if (call.isExecuted() && !call.isCanceled()) {
                call.cancel();
            }
        }
        super.onDestroyView();
    }

    public String getName() {
        return endpoint.getName();
    }

}
