package com.projet.zky.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projet.zky.R;
import com.projet.zky.model.ModelProduct;
import com.projet.zky.util.ImageLoader;
import com.projet.zky.viewHolders.SubFamilyItemViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KevinVi on 31/05/2017.
 */

class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelProduct> products = new ArrayList<>();
    private Context context;

    ProductAdapter(List<ModelProduct> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_categories_item, parent, false);
        return new SubFamilyItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        SubFamilyItemViewHolder catalogueViewHolder = (SubFamilyItemViewHolder) holder;
        catalogueViewHolder.getTextView().setText(products.get(position).getIntitule1());
        ImageLoader.loadImage(context, catalogueViewHolder.getImageView(), products.get(position).getImg());
        catalogueViewHolder.getTextView().setTag(R.string.id_product,products.get(position));
        catalogueViewHolder.getTextView().setTag(R.string.id_products,products);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
