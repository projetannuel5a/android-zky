package com.projet.zky.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projet.zky.R;
import com.projet.zky.model.ModelData;
import com.projet.zky.viewHolders.FamilyViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KevinVi on 31/05/2017.
 */

public class FamilyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelData> families = new ArrayList<>();

    public FamilyAdapter(List<ModelData> families) {
        this.families = families;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_catalogue_endpoint, parent, false);
        return new FamilyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FamilyViewHolder familyViewHolder = (FamilyViewHolder) holder;
        familyViewHolder.getTextView().setText(families.get(position).getFamily().getName());
        familyViewHolder.getTextView().setTag(families.get(position).getSubfamilyPublics());
    }

    @Override
    public int getItemCount() {
        return families.size();
    }
}
