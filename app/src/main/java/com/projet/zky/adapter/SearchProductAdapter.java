package com.projet.zky.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projet.zky.R;
import com.projet.zky.model.ModelProduct;
import com.projet.zky.util.ImageLoader;
import com.projet.zky.viewHolders.SearchProductViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 09/07/2017.
 */

public class SearchProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ModelProduct> products = new ArrayList<>();
    private Context context;

    public SearchProductAdapter(List<ModelProduct> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_search_product, parent, false);
        return new SearchProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SearchProductViewHolder searchProductViewHolder = (SearchProductViewHolder) holder;
        searchProductViewHolder.getProductName().setText(products.get(position).getIntitule1());
        searchProductViewHolder.getProductName().setTag(R.string.id_product, products.get(position));
        searchProductViewHolder.getProductName().setTag(R.string.id_products, products);
        ImageLoader.loadImage(context, searchProductViewHolder.getImgProduct(), products.get(position).getImg());

    }

    @Override
    public int getItemCount() {
        return products.size();
    }


}
