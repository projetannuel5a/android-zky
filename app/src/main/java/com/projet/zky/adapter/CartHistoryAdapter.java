package com.projet.zky.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projet.zky.R;
import com.projet.zky.model.ModelCartHistory;
import com.projet.zky.viewHolders.CartLineViewHolder;

import java.util.List;

/**
 * Created by Kevin on 09/07/2017.
 */

public class CartHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelCartHistory> cartHistories;
    private Context context;
    private int expandedPosition = -1;


    public CartHistoryAdapter(List<ModelCartHistory> cartHistories, Context context) {
        this.cartHistories = cartHistories;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cart_history, parent, false);
        return new CartLineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int pos) {

        final int position = pos;
        CartLineViewHolder cartLineViewHolder = (CartLineViewHolder) holder;

        cartLineViewHolder.getCartId().setText("Parnier : " + position);

        if (position == expandedPosition) {

            cartLineViewHolder.getListCartLine().removeAllViews();
            for (int i = 0; i < cartHistories.get(position).getCartLinePublics().size(); i++) {
                final View itemCarline = LayoutInflater.from(context)
                        .inflate(R.layout.row_cart_line_history, cartLineViewHolder.getListCartLine(), false);
                ((AppCompatTextView) itemCarline.findViewById(R.id.quantity_item))
                        .setText(String.valueOf(cartHistories.get(position).getCartLinePublics().get(i).getQuantity()));

                ((AppCompatTextView) itemCarline.findViewById(R.id.product_item))
                        .setText(cartHistories.get(position).getCartLinePublics().get(i).getProductPublics().getIntitule1());
                int quantity = cartHistories.get(position).getCartLinePublics().get(i).getQuantity();
                float price = Float.valueOf(cartHistories.get(position).getCartLinePublics().get(i).getProductPublics().getPrixdevente());
                float somme = quantity * price;
                ((AppCompatTextView) itemCarline.findViewById(R.id.price_item))
                        .setText(String.valueOf(somme));
                cartLineViewHolder.getListCartLine().addView(itemCarline);
            }
            cartLineViewHolder.getListCartLine().setVisibility(View.VISIBLE);
        } else {
            cartLineViewHolder.getListCartLine().setVisibility(View.GONE);
        }
        cartLineViewHolder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == expandedPosition) {
                    int prev = expandedPosition;
                    expandedPosition = -1;
                    notifyItemChanged(prev);
                } else {
                    if (expandedPosition >= 0) {
                        int prev = expandedPosition;
                        notifyItemChanged(prev);
                    }
                    expandedPosition = position;
                    notifyItemChanged(expandedPosition);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartHistories.size();
    }

}
