package com.projet.zky.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.projet.zky.R;
import com.projet.zky.interfaces.EndpointInterface;
import com.projet.zky.model.CartLinePublic;
import com.projet.zky.model.ModelCartHistory;
import com.projet.zky.util.Shared;
import com.projet.zky.util.Utils;
import com.projet.zky.viewHolders.CartViewHolder;

import java.text.DecimalFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kevin on 20/06/2017.
 */

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ModelCartHistory items;
    private Context context;
    private AppCompatTextView tv;
    private AppCompatButton button;


    public CartAdapter(ModelCartHistory items, Context context, AppCompatTextView tv, AppCompatButton button) {
        this.items = items;
        this.context = context;
        this.tv = tv;
        this.button = button;
        updateQuantity();
        setupButton();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cart_endpoint, parent, false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int pos) {

        final int position = pos;
        final CartViewHolder cartViewHolder = (CartViewHolder) holder;
        cartViewHolder.getTitleCart().setText(items.getCartLinePublics().get(position).getProductPublics().getIntitule1());
        cartViewHolder.getQuantity().setText(String.valueOf(items.getCartLinePublics().get(position).getQuantity()));

        if (items.getCartLinePublics().get(position).getQuantity() > 0) {
            cartViewHolder.getRemoveCart().setEnabled(true);
        }

        cartViewHolder.getAddCart().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartViewHolder.getRemoveCart().setEnabled(true);
                int quantity = Integer.valueOf(cartViewHolder.getQuantity().getText().toString());
                cartViewHolder.getQuantity().setText(String.valueOf(++quantity));
                items.getCartLinePublics().get(position).setQuantity(quantity);
                updateQuantity();
                updateCart(context, quantity, items.getCartLinePublics().get(position).getProductPublics().getId());
            }
        });

        cartViewHolder.getRemoveCart().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.valueOf(cartViewHolder.getQuantity().getText().toString());
                cartViewHolder.getQuantity().setText(String.valueOf(--quantity));
                items.getCartLinePublics().get(position).setQuantity(quantity);
                if (quantity == 0) {
                    cartViewHolder.getRemoveCart().setEnabled(false);
                    updateCart(context, quantity, items.getCartLinePublics().get(position).getProductPublics().getId());
                } else {
                    updateQuantity();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.getCartLinePublics().size();
    }

    private void updateCart(final Context context, int quantity, int id) {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .client(Utils.baseConnection(context, "cart"))
                .baseUrl(context.getString(R.string.url_data))
                .addConverterFactory(GsonConverterFactory.create());
        EndpointInterface getInterface = retrofit.build().create(EndpointInterface.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("productId", id);
        jsonObject.addProperty("quantity", quantity);
        JsonObject obj = new JsonObject();
        obj.addProperty("token", Shared.getToken(context));
        obj.add("body", jsonObject);
        Call<ModelCartHistory> call;
        call = getInterface.setCart(obj);
        call.enqueue(new Callback<ModelCartHistory>() {
            @Override
            public void onResponse(Call<ModelCartHistory> call, Response<ModelCartHistory> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, R.string.retry_later, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelCartHistory> call, Throwable t) {
                Toast.makeText(context, R.string.retry_later, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateQuantity() {
        float total = 0;
        for (CartLinePublic c :
                items.getCartLinePublics()) {
            total += c.getQuantity() * Float.valueOf(c.getProductPublics().getPrixdevente());
        }
        DecimalFormat df = new DecimalFormat("#.##");
        tv.setText(context.getString(R.string.total_price_product, String.valueOf(df.format(total))));

    }

    private void setupButton() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit.Builder retrofit = new Retrofit.Builder()
                        .client(Utils.baseConnection(context, "cart"))
                        .baseUrl(context.getString(R.string.url_data))
                        .addConverterFactory(GsonConverterFactory.create());
                EndpointInterface getInterface = retrofit.build().create(EndpointInterface.class);
                JsonObject obj = new JsonObject();
                obj.addProperty("token", Shared.getToken(context));
                Call<Boolean> call;
                call = getInterface.validate(obj);
                call.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(context, R.string.cart_saved, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.retry_later, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        Toast.makeText(context, R.string.retry_later, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
