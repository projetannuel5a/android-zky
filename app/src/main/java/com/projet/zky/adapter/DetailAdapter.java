package com.projet.zky.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.projet.zky.R;
import com.projet.zky.interfaces.EndpointInterface;
import com.projet.zky.model.ModelCartHistory;
import com.projet.zky.model.ModelProduct;
import com.projet.zky.util.ImageLoader;
import com.projet.zky.util.NullAdapter;
import com.projet.zky.util.Shared;
import com.projet.zky.util.Utils;
import com.projet.zky.viewHolders.DetailViewHolder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kevin on 03/06/2017.
 */

public class DetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ModelProduct product;
    private List<ModelProduct> products = new ArrayList<>();
    private Context context;
    DecimalFormat df = new DecimalFormat("#.##");


    public DetailAdapter(ModelProduct product, List<ModelProduct> products, Context context) {
        this.product = product;
        this.context = context;
        this.products = products;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_detail_endpoint, parent, false);
        return new DetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final DetailViewHolder detailViewHolder = (DetailViewHolder) holder;
        detailViewHolder.getTextView().setText(product.getIntitule1());
        ImageLoader.loadImage(context, detailViewHolder.getImageDetail(), product.getImg());
        detailViewHolder.getRecyclerView().setAdapter(new NullAdapter());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        detailViewHolder.getRecyclerView().setHasFixedSize(true);
        detailViewHolder.getRecyclerView().setLayoutManager(linearLayoutManager);
        detailViewHolder.getRecyclerView().setAdapter(new ProductAdapter(products, context));

        detailViewHolder.getPrice().setText(context.getString(R.string.price_product, product.getPrixdevente()));
        detailViewHolder.getAddCart().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Shared.getToken(context).isEmpty()) {
                    Toast.makeText(context, R.string.must_be_logged, Toast.LENGTH_SHORT).show();
                } else {
                    detailViewHolder.getRemoveCart().setEnabled(true);
                    int quantity = Integer.valueOf(detailViewHolder.getQuantity().getText().toString());
                    detailViewHolder.getQuantity().setText(String.valueOf(++quantity));
                    float total = quantity * Float.valueOf(product.getPrixdevente());
                    detailViewHolder.getTotal().setText(context.getString(R.string.total_price_product, String.valueOf(df.format(total))));
                }
            }
        });

        detailViewHolder.getRemoveCart().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.valueOf(detailViewHolder.getQuantity().getText().toString());
                detailViewHolder.getQuantity().setText(String.valueOf(--quantity));
                if (quantity == 0) {
                    detailViewHolder.getRemoveCart().setEnabled(false);
                    detailViewHolder.getTotal().setText(context.getString(R.string.price_zero));
                } else {
                    float total = quantity * Float.valueOf(product.getPrixdevente());
                    detailViewHolder.getTotal().setText(context.getString(R.string.total_price_product, String.valueOf(df.format(total))));
                }
            }
        });

        detailViewHolder.getButtonCart().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.valueOf(detailViewHolder.getQuantity().getText().toString());
                if (quantity > 0) {
                    updateCart(context, quantity, product.getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    private void updateCart(final Context context, int quantity, int id) {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .client(Utils.baseConnection(context, "cart"))
                .baseUrl(context.getString(R.string.url_data))
                .addConverterFactory(GsonConverterFactory.create());
        EndpointInterface getInterface = retrofit.build().create(EndpointInterface.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("productId", id);
        jsonObject.addProperty("quantity", quantity);
        JsonObject obj = new JsonObject();
        obj.addProperty("token", Shared.getToken(context));
        obj.add("body", jsonObject);
        Call<ModelCartHistory> call;
        call = getInterface.addCart(obj);
        call.enqueue(new Callback<ModelCartHistory>() {
            @Override
            public void onResponse(Call<ModelCartHistory> call, Response<ModelCartHistory> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, R.string.cart_update, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, R.string.retry_later, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelCartHistory> call, Throwable t) {
                Toast.makeText(context, R.string.retry_later, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
