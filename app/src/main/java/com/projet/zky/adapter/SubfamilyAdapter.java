package com.projet.zky.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projet.zky.R;
import com.projet.zky.model.ModelSubFamilyPublic;
import com.projet.zky.util.NullAdapter;
import com.projet.zky.viewHolders.SubFamilyViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KevinVi on 31/05/2017.
 */

public class SubfamilyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ModelSubFamilyPublic> subfamily = new ArrayList<>();

    public SubfamilyAdapter(Context context, List<ModelSubFamilyPublic> subfamily) {
        this.context = context;
        this.subfamily = subfamily;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_categories_endpoint, parent, false);
        return new SubFamilyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        SubFamilyViewHolder subFamilyViewHolder = (SubFamilyViewHolder) holder;
        subFamilyViewHolder.getTextView().setText(subfamily.get(position).getSubFamily().getName());
        subFamilyViewHolder.getRecyclerView().setAdapter(new NullAdapter());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        subFamilyViewHolder.getRecyclerView().setHasFixedSize(true);
        subFamilyViewHolder.getRecyclerView().setLayoutManager(linearLayoutManager);
        subFamilyViewHolder.getRecyclerView().setAdapter(new ProductAdapter(subfamily.get(position).getProducts(), context));
    }

    @Override
    public int getItemCount() {
        return subfamily.size();
    }
}
