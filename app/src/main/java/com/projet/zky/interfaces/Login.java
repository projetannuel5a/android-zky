package com.projet.zky.interfaces;

import com.google.gson.JsonObject;
import com.projet.zky.model.ModelLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by KevinVi on 07/04/2017.
 */

public interface Login {

  @Headers("Content-Type: application/json")
  @POST("authenticate")
  Call<ModelLogin> getLogin(@Body JsonObject jsonObject);
}
