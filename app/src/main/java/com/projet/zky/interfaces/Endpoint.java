package com.projet.zky.interfaces;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by Kevin on 31/05/2017.
 */

public interface Endpoint{
    String getName();

    void callData(View rootView, Fragment fragmentActivity
    );

}
