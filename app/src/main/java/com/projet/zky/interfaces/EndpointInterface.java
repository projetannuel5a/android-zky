package com.projet.zky.interfaces;

import com.google.gson.JsonObject;
import com.projet.zky.model.ModelCartHistory;
import com.projet.zky.model.ModelVersion;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by KevinVi on 07/04/2017.
 */

public interface EndpointInterface {

  @Headers("Content-Type: application/json")
  @POST("product/version")
  Call<ModelVersion> getVerson(@Body JsonObject jsonObject);


  @Headers("Content-Type: application/json")
  @POST("cart")
  Call<ModelCartHistory> getCurrentCart(@Body JsonObject jsonObject);


  @Headers("Content-Type: application/json")
  @POST("cart/history")
  Call<List<ModelCartHistory>> getCartHistory(@Body JsonObject jsonObject);


  @Headers("Content-Type: application/json")
  @POST("cart/set")
  Call<ModelCartHistory> setCart(@Body JsonObject jsonObject);

  @Headers("Content-Type: application/json")
  @POST("cart/add")
  Call<ModelCartHistory> addCart(@Body JsonObject jsonObject);

  @Headers("Content-Type: application/json")
  @POST("cart/validate")
  Call<Boolean> validate(@Body JsonObject jsonObject);
}
